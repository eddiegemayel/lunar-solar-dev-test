import postcss from "rollup-plugin-postcss";

const format = "iife";

const createPlugins = (stylesheet) => {

  const plugins = [];
  if (stylesheet) {
    plugins.push(
      postcss({
        extract: stylesheet,
      })
    );
  }
  return plugins;
};

export default [
  {
    input: "scripts/theme.js",
    output: {
      file: "assets/theme.js",
      format,
    },
    plugins: createPlugins("custom.css"),
  }
];