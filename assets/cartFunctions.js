function removeCartItem() {
  document.querySelectorAll('.icon-close').forEach(icon => {
    icon.addEventListener('click', event => {
      let removeVariant = icon.getAttribute("data-variant-id-remove");

      $.ajax({
        type: "POST",
        url: "/cart/change.js",
        dataType: "json",
        data: {
          id: removeVariant,
          quantity: 0,
        },
        }).then((data) => {
        $.getJSON('/cart.js', function(cart) {

          //update the flyout cart
          //remove cart item clicked on
          let iconParent = icon.parentElement.parentElement;
          iconParent.remove();


          //update and format new subtotal
          let subTotalOriginal = String(cart.items_subtotal_price);
          let subTotalFixed = formatPrice(subTotalOriginal);
          document.querySelector(".cart-flyout--price").innerHTML = "$" + subTotalFixed;

          //if carts empty, let's display the empty message set in the Liquid settings
          if(cart.items_subtotal_price === 0) {
            let cartEmptyDiv = createElement("p", "cart-flyout--text");
            let emptyMessage = document.querySelector(atcSelectors.emptyMessage).getAttribute("data-empty-message");
            cartEmptyDiv.innerHTML = emptyMessage;
            let cartFlyoutContainer = document.querySelector(atcSelectors.cartFlyoutContainer);

            removeElementsByClass("cart-flyout--item-wrap");
            document.querySelector(".cart-flyout--hr").remove();
            document.querySelector(".cart-flyout--bottom").remove();
            cartFlyoutContainer.appendChild(cartEmptyDiv);
          }
        });
      });
    });
  });
}

function quantityUpdate() {
  document.querySelectorAll('.icon-plus').forEach(icon => {
    icon.addEventListener('click', event => {
      let variant = icon.getAttribute("data-variant-id-plus");
      let quantitySpan = icon.previousElementSibling;
      let variantQuantity = quantitySpan.innerHTML;
      variantQuantity ++;
      console.log(variantQuantity);

      icon.setAttribute("data-variant-quantity-number", variantQuantity);

      $.ajax({
        type: "POST",
        url: "/cart/change.js",
        dataType: "json",
        data: {
          id: variant,
          quantity: variantQuantity,
        },
        }).then((data) => {
        $.getJSON('/cart.js', function(cart) {

          quantitySpan.innerHTML = variantQuantity;

          //update and format new subtotal
          let subTotalOriginal = String(cart.items_subtotal_price);
          let subTotalFixed = formatPrice(subTotalOriginal);
          document.querySelector(".cart-flyout--price").innerHTML = "$" + subTotalFixed;
        });
      });
    });
  });

  document.querySelectorAll('.icon-minus').forEach(icon => {
    icon.addEventListener('click', event => {
      let variant = icon.getAttribute("data-variant-id-minus");
      let quantitySpan = icon.nextElementSibling;
      let variantQuantity = quantitySpan.innerHTML;
      console.log(variantQuantity);

      if(variantQuantity > 1) {
        variantQuantity --;

        icon.setAttribute("data-variant-quantity-number", variantQuantity);

        $.ajax({
          type: "POST",
          url: "/cart/change.js",
          dataType: "json",
          data: {
            id: variant,
            quantity: variantQuantity,
          },
          }).then((data) => {
          $.getJSON('/cart.js', function(cart) {

            quantitySpan.innerHTML = variantQuantity;

            //update and format new subtotal
            let subTotalOriginal = String(cart.items_subtotal_price);
            let subTotalFixed = formatPrice(subTotalOriginal);
            document.querySelector(".cart-flyout--price").innerHTML = "$" + subTotalFixed;

          });
        }); 
      }
    });
  });
}

function createElement(element, className) {
  let ele = document.createElement(element);

  if(className) {
    ele.classList.add(className);
  }

  return ele;
}

function removeElementsByClass(className) {
  let elements = document.getElementsByClassName(className);
  while(elements.length > 0) {
    elements[0].parentNode.removeChild(elements[0]);
  }
}

function formatPrice(price) {
  let newPrice = price.substring(0, price.length-2) + "." + price.substring(price.length-2);

  return newPrice;
}