
const menuSelectors = {
  headerMenu: '.header-menu',
  headerLabel: '.header-menu--label',
  menuOpen: '[data-menu-open]',
  menuClose: '[data-menu-close]',
  headerFlyout: '.header-flyout'
}

//Link list flyout
if(document.querySelector(menuSelectors.headerMenu)) {

  let headerMenu = document.querySelector(menuSelectors.headerMenu);
  let headerFlyout = document.querySelector(menuSelectors.headerFlyout);
  let menuOpen = document.querySelector(menuSelectors.menuOpen);
  let menuClose = document.querySelector(menuSelectors.menuClose);

  headerMenu.addEventListener("click", function(){
    if(headerFlyout.classList.contains("header-flyout--open")) {
      headerFlyout.classList.remove("header-flyout--open");
      headerMenu.classList.remove("header-menu--open");
      menuOpen.classList.remove("menu-label--hidden");
      menuClose.classList.add("menu-label--hidden");
      headerFlyout.setAttribute("aria-expanded", "false");
    }
    else {
      headerMenu.classList.add("header-menu--open");
      headerFlyout.classList.add("header-flyout--open");
      menuOpen.classList.add("menu-label--hidden");
      menuClose.classList.remove("menu-label--hidden");
      headerFlyout.setAttribute("aria-expanded", "true")
    }
  });
}