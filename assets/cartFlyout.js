const cartSelectors = {
  headerCart: '.header-cart',
  cartFlyout: '.cart-flyout',
  iconArrow: '.icon-arrow'
}

if(document.querySelector(cartSelectors.headerCart)) {

  let headerCart = document.querySelector(cartSelectors.headerCart);
  let cartFlyout = document.querySelector(cartSelectors.cartFlyout);
  let iconArrow = document.querySelector(cartSelectors.iconArrow);
  
  headerCart.addEventListener("click", function(){
    cartFlyout.classList.add("cart-flyout--open");
    cartFlyout.setAttribute("aria-expanded", "true")
  });

  iconArrow.addEventListener("click", function(){
    cartFlyout.classList.remove("cart-flyout--open");
    cartFlyout.setAttribute("aria-expanded", "false");
  });
}