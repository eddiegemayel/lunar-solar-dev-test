const atcSelectors = {
  atcButton : '[data-ajax-atc]',
  cartFlyoutContainer : '.cart-flyout--container',
  cartFlyoutAdd : ".cart-flyout",
  cartFlyoutText : ".cart-flyout--text",
  emptyMessage: "[data-empty-message]"
}

document.addEventListener("DOMContentLoaded", function() {
  removeCartItem();
  quantityUpdate();

  if(document.querySelector(atcSelectors.atcButton)) {
    let atcButton = document.querySelector(atcSelectors.atcButton);

    //atc click event listener
    atcButton.addEventListener("click", function(){
      let variantIdFirst = atcButton.getAttribute("data-variant-id-first");
      let variantIdSecond = atcButton.getAttribute("data-variant-id-second");
      let variantIdThird = atcButton.getAttribute("data-variant-id-third");

      data = {
        items: [
          {
            id: variantIdFirst,
            quantity: 1
          },
          {
            id: variantIdSecond,
            quantity: 1
          },
          {
            id: variantIdThird,
            quantity: 1
          }
        ]
      }

      //post to the cart api
      $.ajax({
        type: 'POST',
        url: '/cart/add.js',
        data: data,
        dataType: 'json',
        success: function() { 
          let cartFlyoutAdd = document.querySelector(atcSelectors.cartFlyoutAdd);

          if(!cartFlyoutAdd.classList.contains("cart-flyout--open")) {
            cartFlyoutAdd.classList.add("cart-flyout--open");
            cartFlyoutAdd.setAttribute("aria-expanded", "true");
          }

          //get updated cart contents
          $.getJSON('/cart.js', function(cart) {
            let cartFlyoutContainer = document.querySelector(atcSelectors.cartFlyoutContainer);

            // if cart was updated, let's clear the out of date items
            if(document.querySelector(".cart-flyout--item-wrap")) {
              removeElementsByClass("cart-flyout--item-wrap");
            }

            //loop through updated cart items
            //and build out any new cart contents
            for (let i = 0; i < cart.items.length; i++) {
              let newCartItem = createElement("div", "cart-flyout--item-wrap");

              let newCartLeftDiv = createElement("div");
              let newCartImage = createElement("img", "cart-flyout--item-image");
              newCartImage.src = cart.items[i].image;
              newCartLeftDiv.appendChild(newCartImage);

              let newCartMiddleDiv = createElement("div");
              let newCartItemTitle = createElement("p", "cart-flyout--item-title");
              let itemTitle = cart.items[i].title;
              newCartItemTitle.innerHTML = itemTitle;
              newCartMiddleDiv.appendChild(newCartItemTitle);

              let newCartItemQuantity = createElement("p", "cart-flyout--item-details");
              let priceOriginal = String(cart.items[i].price);
              let priceFixed = formatPrice(priceOriginal);

              let newCartItemQuantitySelect = createElement("div", "cart-flyout--quantity");
              let iconAction = createElement("span", "icon-action");
              iconAction.setAttribute("data-variant-quantity-number", cart.items[i].quantity);
              iconAction.innerHTML = cart.items[i].quantity;
              newCartItemQuantitySelect.innerHTML = '<svg class="icon-minus" data-variant-id-minus="'+ cart.items[i].id +'" viewBox="0 0 8 1" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 0.5H7" stroke="#2B2B2B" stroke-linecap="square"/></svg> ';
              newCartItemQuantitySelect.appendChild(iconAction);
              newCartItemQuantitySelect.innerHTML += ' <svg class="icon-plus" data-variant-id-plus="'+ cart.items[i].id +'" viewBox="0 0 9 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.5 5.00014H8.5" stroke="#2B2B2B" stroke-linecap="square"/><path d="M4.5 1L4.5 9" stroke="#2B2B2B" stroke-linecap="square"/></svg>';

              if(itemTitle.includes("Pack")) {
                newCartItemQuantity.innerHTML = "Pack of 20, ";
              }
              else {
                newCartItemQuantity.innerHTML = "Single bottle, ";
              }

              newCartItemQuantity.innerHTML += "$" + priceFixed  + " each";
              newCartMiddleDiv.appendChild(newCartItemQuantity);
              newCartMiddleDiv.appendChild(newCartItemQuantitySelect);

              let newCartItemRemove = createElement("div", "cart-flyout--remove");
              newCartItemRemove.innerHTML = '<svg class="icon-close" data-variant-id-remove='+ cart.items[i].id +' viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2.315L20.27 20.585" stroke="#272727" stroke-width="2" stroke-linecap="square"/><path d="M20.27 2.315L2.00002 20.585" stroke="#272727" stroke-width="2" stroke-linecap="square"/></svg>';
              
              newCartItem.appendChild(newCartLeftDiv);
              newCartItem.appendChild(newCartMiddleDiv);
              newCartItem.appendChild(newCartMiddleDiv);
              newCartItem.appendChild(newCartItemRemove);

              if(document.querySelector(".cart-flyout--bottom")) {
                cartFlyoutContainer.prepend(newCartItem);
              }
              else {
                cartFlyoutContainer.appendChild(newCartItem);
              }
            }

            //remove empty cart text if it's there
            if(document.querySelector(".cart-flyout--text")) {
              let cartFlyoutText = document.querySelector(atcSelectors.cartFlyoutText);
              cartFlyoutText.remove();
            }

            if(document.querySelector(".cart-flyout--bottom") == null) {
              let newCartHr = createElement("hr", "cart-flyout--hr");

              let newCartBottom = createElement("div", "cart-flyout--bottom");

              let newCartSubtotal = createElement("span", "cart-flyout--subtotal");
              newCartSubtotal.innerHTML = "Subtotal:";

              let newCartSubtotalPrice = createElement("span", "cart-flyout--price");
              let subTotalOriginal = String(cart.items_subtotal_price);
              let subTotalFixed = formatPrice(subTotalOriginal);
              newCartSubtotalPrice.innerHTML = "$" + subTotalFixed;

              newCartBottom.appendChild(newCartSubtotal);
              newCartBottom.appendChild(newCartSubtotalPrice);

              cartFlyoutContainer.appendChild(newCartHr);
              cartFlyoutContainer.appendChild(newCartBottom);
            }
            else {
              let subTotalOriginal = String(cart.items_subtotal_price);
              let subTotalFixed = formatPrice(subTotalOriginal);
              document.querySelector(".cart-flyout--price").innerHTML = "$" + subTotalFixed;
            }

            //re init remove icon event listeners
            removeCartItem();
            quantityUpdate();
          });
        },
        error: function() { 
          alert("Error, this variant may be out of stock.");
        }
      });
    });
  }
});